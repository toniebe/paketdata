package com.team3.paketdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaketdataApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaketdataApplication.class, args);
	}

}
